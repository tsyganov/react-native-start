import React from 'react';
import {Text, View, StyleSheet, TouchableOpacity} from "react-native";

const PlaceItem = props => (
  <TouchableOpacity onPress={props.placePressed}>
    <View style={styles.placeItem}>
      <Text>{props.placeName}</Text>
    </View>
  </TouchableOpacity>
);

const styles = StyleSheet.create({
  placeItem: {
    width: '100%',
    backgroundColor: '#eee',
    marginBottom: 10,
    padding: 10
  }
});

export default PlaceItem;